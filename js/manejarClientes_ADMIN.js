//FUNCIONES GLOBALES
//Funcion generar ID para clases
function generarId(length) {
    let id = "";
    let characters =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    let charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        id += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return id;
}

//Funciones generales de local storage
function traerLocalStorage(nombreLS) {
    return JSON.parse(localStorage.getItem(nombreLS));
}

function guardarLS(nombreLS, nombreArray) {
    localStorage.setItem(nombreLS, JSON.stringify(nombreArray));
}

//Funciones sweet alert
function sweetAlertSinBtn(icono, mensaje) {
    Swal.fire({
        icon: icono,
        title: mensaje,
        showConfirmButton: false,
        timer: 1500
    });
}

//Conseguir valor del btn del btn-group de '#Membresia'
$(function () {
    // Get click event, assign button to var, and get values from that var
    $('#Membresia button').on('click', function () {
        let thisBtn = $(this);

        thisBtn.addClass('active').siblings().removeClass('active');
        let btnValue = thisBtn.val();
        document.getElementById('Membresia').value = btnValue;
    })
});

//FUNCIONES PAG: MANEJAR CLIENTES
//Class cliente
class cliente {
    constructor(nombre, apellido, dni, telefono, observaciones, membresia) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
        this.telefono = telefono;
        this.observaciones = observaciones;
        this.membresia = membresia;
        this.usuario = dni;
        switch (this.membresia) {
            case 'Bronce': this.horasSemanalesDisponibles = 2; this.membresiaImg = "./img/bronze.png"; break;
            case 'Plata': this.horasSemanalesDisponibles = 3; this.membresiaImg = "./img/silver.png"; break;
            case 'Oro': this.horasSemanalesDisponibles = 4; this.membresiaImg = "./img/oro.png"; break;
            case 'Champion': this.horasSemanalesDisponibles = 6; this.membresiaImg = "./img/champion.png"; break;
        }
        this.id = generarId(20);
        this.pass = generarId(4);
        this.habilitado = true;
        this.horasSemanalesOcupadas = 0;
        this.clasesSemana = [];
    }
}

//Funcion que agrega un nuevo cliente
function crearCliente() {
    let nombre = document.getElementById('Nombre').value;
    let apellido = document.getElementById('Apellido').value;
    let telefono = document.getElementById('Telefono').value;
    let observaciones = document.getElementById('Observaciones').value;
    let membresia = document.getElementById('Membresia').value;
    let dni = document.getElementById('DNI').value;

    let patternNumber = /^[0-9]+$/;
    let patternLetters = /^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/;

    if (nombre === '' || apellido === '' || dni === '' || telefono === '') {
        sweetAlertSinBtn('error', 'Llene todos los campos');
    } else {
        let clientes = traerLocalStorage('clientes');
        if (!clientes) {
            clientes = [];
        }
        let dniExiste = false;
        if (clientes.length > 0) {
            dniExiste = clientes.find(cliente => cliente.dni === dni);
        }
        if (dniExiste) {
            sweetAlertSinBtn('error', 'El usuario ya está en uso');
        } else if (dni.length < 7 || dni.length > 8) {
            sweetAlertSinBtn('error', 'El dni debe tener entre 7 y 8 caracteres');
        } else if (telefono.length < 9 || telefono.length > 11) {
            sweetAlertSinBtn('error', 'El teléfono tiene que tener entre 9 y 11 caracteres');
        } else if (nombre.match(patternLetters) === null || apellido.match(patternLetters) === null || dni.match(patternNumber) === null || telefono.match(patternNumber) === null) {
            return;
        } else {
            observaciones ? observaciones : observaciones = "No hay observaciones";
            let nuevoCliente = new cliente(nombre, apellido, dni, telefono, observaciones, membresia);
            clientes.push(nuevoCliente);
            guardarLS('clientes', clientes);
            sweetAlertSinBtn('success', `Nuevo cliente: ${nombre} ${apellido}`);
            cerrarYvaciarAddClientModal();
            listarClientes();
        }
    }
}

//Funcion que cierra y vacia modal de AddCliente
function cerrarYvaciarAddClientModal() {
    $('#AddClient').modal('hide');
    document.getElementById('Nombre').value = "";
    document.getElementById('Apellido').value = "";
    document.getElementById('Telefono').value = "";
    document.getElementById('Observaciones').value = "";
    document.getElementById('Membresia').value = "";
    document.getElementById('DNI').value = "";
    limpiarMembresias();
    esconderSpansError()
}

//Funcion que resetea la semana
function borrarSemanaCompleta() {
    let clientes = traerLocalStorage('clientes');
    clientes.forEach(cliente => {
        cliente.clasesSemana = [];
        cliente.horasSemanalesOcupadas = 0;
    });
    guardarLS('clientes', clientes);
    localStorage.removeItem('clases');
    sweetAlertSinBtn('success', 'Semana reseteada');
    $('#AddReset').modal('hide');
}

//Listar clientes
function listarClientes() {
    let listadoClientes = traerLocalStorage('clientes');
    if (!listadoClientes) {
        listadoClientes = [];
    }
    if (listadoClientes.length == 0) {
        sweetAlertSinBtn('info', 'No tiene clientes creados');
    } else {
        let contenedorListaClientes = document.getElementById('listadoClientes');
        contenedorListaClientes.innerHTML = "";
        for (let index = 0; index < listadoClientes.length; index++) {
            let cliente = listadoClientes[index];
            let card = document.createElement('div');
            card.setAttribute("class", "col mb-5");
            card.innerHTML = crearCardCliente(cliente);
            contenedorListaClientes.appendChild(card);
            cambiarHabilitado_Suspendido(cliente.id, cliente.habilitado);
        }
    }
}

//Funcion que crea una card con datos del cliente para ListarClientes
function crearCardCliente(cliente) {
    let nombre = cliente.nombre;
    let apellido = cliente.apellido;
    let telefono = cliente.telefono;
    let dni = cliente.dni;
    let membresiaImg = cliente.membresiaImg;
    let id = cliente.id;
    let observaciones = cliente.observaciones;
    return `
    <div class="col mb-5">
        <div class="card h-100" style="box-shadow: 2px 2px 3px 0px rgba(163,163,163,0.75);">
            <div style="max-width: 540px;">
                <div class="row no-gutters">
                    <div class="col-4 col-lg-4 col-md-12" style="background-image: url(./img/marco.png);">
                        <img src="./img/folder.png"  max class="img-fluid mt-5" alt="...">
                    </div>
                <div class="col-8 col-lg-8 col-md-12" style="background-color: #ffffff;">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist" style="background-color: #ed9239;">
                            <a class="nav-item nav-link active" id="nav-home-tab-${id}" data-toggle="tab" href="#nav-home-${id}" role="tab" aria-controls="nav-home-${id}" aria-selected="true">
                                <img src="./img/xbox.png" />
                            </a>
                            <a class="nav-item nav-link" id="nav-profile-tab-${id}" data-toggle="tab" href="#nav-profile-${id}" role="tab" aria-controls="nav-profile-${id}" aria-selected="false">
                                <img src="./img/laptop.png" />
                            </a>
                            <a class="nav-item nav-link" id="nav-contact-tab-${id}" data-toggle="tab" href="#nav-contact-${id}" role="tab" aria-controls="nav-contact-${id}" aria-selected="false">
                                <img src="./img/hp.png" />
                            </a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-home-${id}" role="tabpanel" aria-labelledby="nav-home-${id}-tab-${id}">
                            <div class="card-body">
                                <p class="card-title">${nombre} ${apellido}</p>
                                <!--Condición-->
                                <div id="btn-membresia">
                                    <button type="button" class="btn text-success" id="btnHabilitado-${id}" onclick="habilitarUsuario('${id}')">
                                        <i class="fas fa-gamepad" style="font-size: 30px;"></i> Activado
                                    </button>
                                    <button type = "button"  class="btn text-danger" id="btn-suspendido-${id}" onclick="suspenderUsuario('${id}')">
                                        <i class="fas fa-skull-crossbones" style="font-size: 30px;"></i> Suspendido
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade text-center mt-2 mb-3" id="nav-profile-${id}" role="tabpanel" aria-labelledby = "nav-profile-tab-${id}">
                            <i class = "fas fa-id-card mt-2 mr-2" style="font-size: 20px;"></i>${dni}<br>
                            <i class = "fas fa-phone mt-2 mr-2 " style="font-size: 20px;"></i>${telefono}<br>
                            <img src = "${membresiaImg}" class="img-fluid mt-1 mb-1" alt="Membresia" style = "max-height: 72px; max-width: 72px;">
                        </div>
                        <div class="tab-pane fade mt-4 text-center" id = "nav-contact-${id}" role="tabpanel" aria-labelledby = "nav-contact-tab-${id}">
                            <i class = "fas fa-first-aid mt-4 mr-2" style = "font-size: 30px;"></i>
                            <div class = "mt-3 mb-5">${observaciones}</div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>`;
}

//Funcion cambiar btnhabilitado/btnsuspendido: recibe cliente id y clienteHabilitado por parametro
function cambiarHabilitado_Suspendido(idCliente, clienteHabilitado) {
    let btnHabilitado = `btnHabilitado-${idCliente}`;
    let btnSuspendido = `btn-suspendido-${idCliente}`;
    if (clienteHabilitado) {
        document.getElementById(btnHabilitado).disabled = true;
        document.getElementById(btnHabilitado).setAttribute("class", "btn border border-success");
        document.getElementById(btnSuspendido).disabled = false;
        document.getElementById(btnSuspendido).setAttribute("class", "btn text-danger");
    } else {
        document.getElementById(btnHabilitado).disabled = false;
        document.getElementById(btnHabilitado).setAttribute("class", "btn text-success");
        document.getElementById(btnSuspendido).disabled = true;
        document.getElementById(btnSuspendido).setAttribute("class", "btn border border-danger");
    }
}

//Funcion que cambia la habilitacion del usuario a true
function habilitarUsuario(id) {
    let listadoClientes = traerLocalStorage('clientes');
    let indexCliente = listadoClientes.findIndex(cliente => cliente.id === id);
    let cliente = listadoClientes[indexCliente];
    cliente.habilitado = true;
    guardarLS('clientes', listadoClientes);
    sweetAlertSinBtn('success', `Usuario ${cliente.nombre} ${cliente.apellido} habilitado`);
    listarClientes();
}

//Funcion que cambia la habilitacion del usuario a false
function suspenderUsuario(id) {
    let listadoClientes = traerLocalStorage('clientes');
    let indexCliente = listadoClientes.findIndex(cliente => cliente.id === id);
    let cliente = listadoClientes[indexCliente];
    cliente.habilitado = false;
    guardarLS('clientes', listadoClientes);
    sweetAlertSinBtn('success', `Usuario ${cliente.nombre} ${cliente.apellido} suspendido`);
    listarClientes();
}

listarClientes();

function checkUserIsLogged() {
    let usuarioLogueado = traerLocalStorage('usuarioLogueado');
    if (!usuarioLogueado) {
        sweetAlertSinBtn("error", "Debes estar logueado para ingresar");
        setTimeout(() => location.href = "login.html", 700);
    }
}

document.querySelector("#DNI").addEventListener("input", () => {
    let dni = document.getElementById("DNI").value;
    let letters = /^[0-9]+$/;
    if (dni.match(letters) === null && dni !== "" && document.querySelector("#userAlertDNI").classList.value.search("d-relative") === -1) {
        document.querySelector("#userAlertDNI").classList.toggle("d-relative");
        document.querySelector("#userAlertDNI").classList.toggle("d-none");
    } else if (dni.match(letters) !== null && dni !== "" && document.querySelector("#userAlertDNI").classList.value.search("d-relative") !== -1) {
        document.querySelector("#userAlertDNI").classList.toggle("d-relative");
        document.querySelector("#userAlertDNI").classList.toggle("d-none");
    } else if (dni === "" && document.querySelector("#userAlertDNI").classList.value.search("d-relative") !== -1) {
        document.querySelector("#userAlertDNI").classList.toggle("d-relative");
        document.querySelector("#userAlertDNI").classList.toggle("d-none");
    }
});

document.querySelector("#Telefono").addEventListener("input", () => {
    let telefono = document.getElementById("Telefono").value;
    let letters = /^[0-9]+$/;
    if (telefono.match(letters) === null && telefono !== "" && document.querySelector("#userAlertTelefono").classList.value.search("d-relative") === -1) {
        document.querySelector("#userAlertTelefono").classList.toggle("d-relative");
        document.querySelector("#userAlertTelefono").classList.toggle("d-none");
    } else if (telefono.match(letters) !== null && telefono !== "" && document.querySelector("#userAlertTelefono").classList.value.search("d-relative") !== -1) {
        document.querySelector("#userAlertTelefono").classList.toggle("d-relative");
        document.querySelector("#userAlertTelefono").classList.toggle("d-none");
    } else if (telefono === "" && document.querySelector("#userAlertTelefono").classList.value.search("d-relative") !== -1) {
        document.querySelector("#userAlertTelefono").classList.toggle("d-relative");
        document.querySelector("#userAlertTelefono").classList.toggle("d-none");
    }
});

document.querySelector("#Nombre").addEventListener("input", () => {
    let nombre = document.getElementById("Nombre").value;
    let letters = /^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/;
    if (nombre.match(letters) === null && nombre !== "" && document.querySelector("#userAlertNombre").classList.value.search("d-relative") === -1) {
        document.querySelector("#userAlertNombre").classList.toggle("d-relative");
        document.querySelector("#userAlertNombre").classList.toggle("d-none");
    } else if (nombre.match(letters) !== null && nombre !== "" && document.querySelector("#userAlertNombre").classList.value.search("d-relative") !== -1) {
        document.querySelector("#userAlertNombre").classList.toggle("d-relative");
        document.querySelector("#userAlertNombre").classList.toggle("d-none");
    } else if (nombre === "" && document.querySelector("#userAlertNombre").classList.value.search("d-relative") !== -1) {
        document.querySelector("#userAlertNombre").classList.toggle("d-relative");
        document.querySelector("#userAlertNombre").classList.toggle("d-none");
    }
});

document.querySelector("#Apellido").addEventListener("input", () => {
    let apellido = document.getElementById("Apellido").value;
    let letters = /^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/;
    if (apellido.match(letters) === null && apellido !== "" && document.querySelector("#userAlertApellido").classList.value.search("d-relative") === -1) {
        document.querySelector("#userAlertApellido").classList.toggle("d-relative");
        document.querySelector("#userAlertApellido").classList.toggle("d-none");
    } else if (apellido.match(letters) !== null && apellido !== "" && document.querySelector("#userAlertApellido").classList.value.search("d-relative") !== -1) {
        document.querySelector("#userAlertApellido").classList.toggle("d-relative");
        document.querySelector("#userAlertApellido").classList.toggle("d-none");
    } else if (apellido === "" && document.querySelector("#userAlertApellido").classList.value.search("d-relative") !== -1) {
        document.querySelector("#userAlertApellido").classList.toggle("d-relative");
        document.querySelector("#userAlertApellido").classList.toggle("d-none");
    }
});

//Limpiar membresias
function limpiarMembresias() {
    let btnMembresia = document.querySelectorAll("#Membresia .rounded-pill");
    btnMembresia.forEach(btn => {
        let value = btn.value;
        if (btn.classList.value.search("active") !== -1) {
            document.querySelector(`.btn[value="${value}"]`).classList.toggle("active");
        }
    });
}

function esconderSpansError() {
    let errorSpan = document.querySelectorAll(".row > .col-12 > span");
    errorSpan.forEach(span => {
        if (span.classList.value.search("d-relative") !== -1) {
            document.querySelector(`#${span.id}`).classList.toggle("d-relative");
            document.querySelector(`#${span.id}`).classList.toggle("d-none");
        }
    });
}

$('#AddClient').on('hidden.bs.modal', function () {
    cerrarYvaciarAddClientModal();
})