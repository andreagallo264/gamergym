//Información hardcodeada con datos agregables de día, horarios y salas
<<<<<<< HEAD
var semana = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"];
=======
var semana = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado","Domingo"];
>>>>>>> d998d281b8e4cdcd6ff75982a2e49de265cf396e
var horarios = ["09:00", "10:00", "11:00", "12:00", "13:00", "16:00", "17:00", "18:00", "19:00", "20:00"];
var salas = ["Sala VR", "Sala PC"];

//Funciones generales de local storage
function traerLocalStorage(nombreLS) {
    return JSON.parse(localStorage.getItem(nombreLS));
}

function guardarLS(nombreLS, nombreArray) {
    localStorage.setItem(nombreLS, JSON.stringify(nombreArray));
}

//INICIO TURNO
class turno {
    constructor(dia, hora, juego, sala, cupo) {
        this.id = generarId(20);

        this.dia = dia;
        this.hora = hora;
        this.juego = juego;
        this.sala = sala;
        this.alumnos = [];
        this.cupoDisponible = cupo;
    }
}

function agregarTurno(dia, horario, sala, cupo, imgUrl) {
    let requiereVr = sala == "Sala VR" ? true : false;
    let listaJuegos = traerLocalStorage('juegos');
    if (listaJuegos) {
        let juegos = traerLocalStorage('juegos').filter(juego => requiereVr ? juego.requiereVr === true : juego.requiereVr === false);
        if (juegos && juegos.length >= 1) {
            $('#AddAsignarJuego').modal('toggle');
            $('#AddAsignarJuego').modal('show');
            document.getElementById('diaModal').value = dia;
            document.getElementById('horarioModal').value = horario;
            document.getElementById('salaModal').value = sala;
            document.getElementById('cupoModal').value = cupo;
            document.getElementById('imgUrlModal').value = imgUrl;
            cargarSelect(requiereVr);
        } else {
            sweetAlertSinBtn("error", "No hay juegos creados")
        }

    } else {
        sweetAlertSinBtn("error", "No hay juegos creados")
    }
}

//Listado de juegos en el ADMIN
function listarTurnos() {
    let divTurnos = document.getElementById('turnos');
    divTurnos.innerHTML = '';
    let turnos = traerLocalStorage('turnos');
    if (!turnos) {
        turnos = [];
    }
    let juegos = traerLocalStorage('juegos');
    if (turnos.length > 0) {
        for (let i = 0; i < turnos.length; i++) {
            let img = juegos.find(elemento => elemento.juego == turnos[i].juego).imgURL;
            let card = `<!--Inicio Card-->
        <div class="card text-center bg-primary mx-auto border-0" style="width: 270px;">
          <img src="${img}" alt="" style="height: 12rem; width: 100%;">
          <div class="bg-primary text-white pt-2 pb-1">
            <p>${turnos[i].dia} ${turnos[i].hora}</p>
            <p>${turnos[i].juego}</p>
            <button type='submit' class='btn btn-warning' onclick="eliminarTurnoListado('${turnos[i].id}')"><i class="fas fa-trash"></i></button>
          </div>
        </div>
        <!--Fin Card-->`;
            divTurnos.innerHTML += card;
        }
    } else {
        sweetAlertSinBtn("error", "No tiene turnos creados")
    }
}

//Funcion que elimina los turnos desde la pagina de listarTurnos
function eliminarTurnoListado(id) {
    let turnos = traerLocalStorage('turnos');
    let index = turnos.findIndex(turno => turno.id == id);
    if (turnos[index].alumnos.length == 0) {
        turnos.splice(index, 1);
        guardarLS('turnos', turnos);
        listarTurnos();
        sweetAlertSinBtn('success', 'Turno eliminado');
    } else {
        sweetAlertSinBtn('error', 'No podés borrar un turno con alumnos inscriptos!');
    }

}


//Funcion que crea un nuevo turno desde el acordeon del administrador
function crearTurno() {
    let dia = document.getElementById('diaModal').value
    let horario = document.getElementById('horarioModal').value;
    let sala = document.getElementById('salaModal').value;
    let cupo = document.getElementById('cupoModal').value;
    let juego = document.getElementById('juegos').value;

    let nuevoTurno = new turno(dia, horario, juego, sala, cupo)
    let turnos = traerLocalStorage('turnos');
    turnos = !turnos ? [] : turnos;
    turnos.push(nuevoTurno);
    guardarLS('turnos', turnos);
    sweetAlertSinBtn('success', `Turno guardado: ${dia} - ${horario} - ${juego}`);
    $('#AddAsignarJuego').modal('hide');
    generarAcordeones();
}

//Funcion que trae la informacion del turno segun dia y hora y devuelve los datos de un horario específico
function getTurnos(dia, hora) {
    let turnos = traerLocalStorage('turnos');
    if (!turnos) {
        return false;
    } else {
        let turnosPorDia = turnos.filter(turno => turno.dia == dia);
        if (!turnosPorDia) {
            return false;
        } else {
            let turnosPorHora = turnosPorDia.filter(turno => turno.hora == hora);
            if (!turnosPorHora || turnosPorHora.length == 0) {
                return false;
            } else {
                return turnosPorHora;
            }
        }
    }
}

//Funciones para generar las salas: vacias y llenas, retornan string para asignar al interior de la rowSala del dia
function generarSalaVacia(dia, horario, sala) {
    let cupo = sala == "Sala VR" ? 4 : 30;
    let img = sala == "Sala VR" ? './img/headset.png' : './img/monitor.png';
    let salaVacia = `
    <!--Inicio de sala: ${sala}-->
        <div class="col-sm-6 bg-primary">
            <div class="card">
                <div class="card-body">
                    <img src="${img}" alt="">
                    <h5 class="card-title font-weight-bold mt-2">${sala}</h5>
                    <button class="btn btn-primary border border-dark animated pulse infinite rounded-pill px-5" onclick="agregarTurno('${dia}', '${horario}', '${sala}', '${cupo}')">Agregar</button>
                </div>
            </div>
        </div>
    <!--Fin de Sala: ${sala}-->
    `;
    return salaVacia;
}

//Funcion que genera una sola que ya tiene asignado un juego
function generarSalaOcupada(turno) {
    let img = turno.sala == "Sala VR" ? './img/headset.png' : './img/monitor.png';
    let salaOcupada = `
    <!--Inicio de sala: ${turno.sala}-->
        <div class="col-sm-6 bg-primary">
            <div class="card">
                <div class="card-body">
                    <img src="${img}" alt="">
                    <h5 class="card-title font-weight-bold mt-2">${turno.juego}: ${turno.cupoDisponible} cupo máx.</h5>
                    <button class="btn btn-danger border border-dark animated pulse infinite rounded-pill px-5" onclick="eliminarTurno('${turno.id}')">Eliminar</button>
                </div>
            </div>
        </div>
    <!--Fin de Sala: ${turno.sala}-->
    `;
    return salaOcupada;
}

//Funcion que genera el acordeon con los datos de los turnos de la semana
function generarAcordeones() {
    let divSemana = document.getElementById('semana');
    for (let i = 0; i < semana.length; i++) {
        let acordeon = document.createElement('div');
        acordeon.setAttribute('class', 'accordion');
        let idAcordeon = 'accordion' + semana[i];
        acordeon.setAttribute('id', idAcordeon);
        let idBody = "cardBody" + semana[i];
        let acordeonCard = `
        <!--Inicio de Card ${semana[i]}-->
                <div class="card text-center bg-primary" >
                  <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                      <button class="btn btn-link text-white" onclick="generarSalas('${semana[i]}', '09:00')"  type="button" data-toggle="collapse" data-target="#${semana[i]}" aria-expanded="false" aria-controls="collapseOne">
                        ${semana[i]}
                      </button>
                    </h2>
                  </div>
                  <div id="${semana[i]}" class="collapse" style="background-image: url(./img/pacman.png); background-color: black;" aria-labelledby="headingOne" data-parent="#${idAcordeon}">
                    <div class="card-body" id="${idBody}">
                        
                    </div>
                    <h5 class='text-center text-white' id='tituloHorarios${semana[i]}'>Horario seleccionado: 09:00 hs.</h5>
                    <div class="card card-body" style="background-image: url(./img/pacman.png); background-color: black;">
                        <div class="row" id="rowSalas${semana[i]}">
                        </div>
                    </div>
                  </div>
                </div>
                <!--Final de Card ${semana[i]}-->
        `;
        divSemana.appendChild(acordeon);
        document.getElementById(idAcordeon).innerHTML = acordeonCard;
        generarHorarios(semana[i], idBody);

    }
}

//Funcion que elimina un turno del listado de turnos
function eliminarTurno(id) {
    let turnos = traerLocalStorage('turnos');
    let index = turnos.findIndex(turno => turno.id == id);
    if (turnos[index].alumnos.length == 0) {
        turnos.splice(index, 1);
        guardarLS('turnos', turnos);
        sweetAlertSinBtn('success', 'Turno eliminado');
        generarAcordeones();
    } else {
        sweetAlertSinBtn('error', 'No podés borrar un turno con alumnos inscriptos!');
    }

}


//Funcion que genera los botones de los horarios con las funciones que traen la info de las salas
//Se llama en generarAcordeones
function generarHorarios(dia, idCardBody) {
    let cardBody = document.getElementById(idCardBody);
    for (let i = 0; i < horarios.length; i++) {
        let btn = document.createElement("button");
        btn.innerText = horarios[i];
        btn.setAttribute('class', 'btn btn-primary mx-2 my-1');
        btn.setAttribute('onclick', `generarSalas('${dia}', '${horarios[i]}')`);
        cardBody.appendChild(btn);
    }
}


//Esta función genera las salas segun dia y horario y las inserta en la row correspondiente al dia
function generarSalas(dia, horario) {
    let idSalasBody = "rowSalas" + dia;
    let salasBody = document.getElementById(idSalasBody);
    salasBody.innerHTML = "";
    let salasEnUso = getTurnos(dia, horario);
    if (!salasEnUso || salasEnUso == false) {
        for (let i = 0; i < salas.length; i++) {
            salasBody.innerHTML += generarSalaVacia(dia, horario, salas[i]);
        }
    } else {
        for (let i = 0; i < salas.length; i++) {
            let salaUsandose = salasEnUso.filter(turno => turno.sala == salas[i]);
            salasBody.innerHTML += salaUsandose.length > 0 ? generarSalaOcupada(salaUsandose[0]) : generarSalaVacia(dia, horario, salas[i]);
        }
    }
    let idH5 = 'tituloHorarios' + dia;
    document.getElementById(idH5).innerText = `Horario seleccionado: ${horario} hs.`;
}

function checkUserIsLogged() {
    let usuarioLogueado = traerLocalStorage('usuarioLogueado');
    console.log("eeeh")
    if (!usuarioLogueado) {
        sweetAlertSinBtn("error", "Debes estar logueado para ingresar");
        setTimeout(() => location.href = "login.html", 700);
        console.log('hola')
    }
}