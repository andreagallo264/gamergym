//FUNCIONES ACCESORIAS
//Funciones sweet alert
function sweetAlertSinBtn(icono, mensaje) {
    Swal.fire({
        icon: icono,
        title: mensaje,
        showConfirmButton: false,
        timer: 1500
    });
}

//Funcion que limpia el login
function limpiarLogin() {
    document.getElementById("input_usuario").value = '';
    document.getElementById("input_password").value = '';
}

//Funciones generales de local storage
function traerLocalStorage(nombreLS) {
    return JSON.parse(localStorage.getItem(nombreLS));
}

function guardarLS(nombreLS, nombreArray) {
    localStorage.setItem(nombreLS, JSON.stringify(nombreArray));
}

//Funcion que chequea si el usuario es admin con su pass
function checkIfAdmin(usuario, pass) {
    if (usuario === '1234') {
        if (pass === 'admin') {
            sweetAlertSinBtn('success', 'Te has logueado como Administrador');
            limpiarLogin();
            let usuarioLogueado = traerLocalStorage('usuarioLogueado');
            if (!usuarioLogueado) {
                usuarioLogueado = [];
            }
            usuarioLogueado.push(1234);
            guardarLS('usuarioLogueado', usuarioLogueado);
            setTimeout(() => location.href = "agregarCliente.html", 1000);
            return true;
        } else {
            sweetAlertSinBtn('error', 'Usuario o contraseña incorrectos');
            limpiarLogin();
            return true;
        }
    } else {
        return false;
    }
}

//Funcion que chequea si los inputs están vacíos
function checkInputs(usuario, pass) {
    if (usuario === undefined || usuario === null || usuario === '' || pass === undefined || pass === null || pass === '') {
        sweetAlertSinBtn('error', 'Llene todos los campos');
        limpiarLogin();
        return false;
    } else {
        return true;
    }
}

//Funcion que chequea si es cliente el que se loguea. Devuelve el id del usuario si pasa las comprobaciones. True si existe el usuario
//pero erró la contraseña y false si el usuario no existe.

function checkIfClientExists(usuario, pass) {
    let listadoClientes = traerLocalStorage('clientes');
    //No hay clientes almacenados
    if (!listadoClientes) {
        sweetAlertSinBtn('error', 'No hay clientes registrados');
        limpiarLogin();
        return false;
    } else {
        let indexCliente = listadoClientes.findIndex(cliente => cliente.usuario === usuario);
        //Cliente existe
        if (indexCliente !== -1) {
            //Es Primer login
            if (listadoClientes[indexCliente].pass.length === 4 && listadoClientes[indexCliente].pass === pass) {
                document.getElementById('form_login').innerHTML = `<label class="mb-2" style="font-family: gamer;" for="input_nuevaPass">Nueva Contraseña</label>
                <br>
                <input placeholder="******" type="password" id="input_nuevaPass">
                <br>
                <button class="btn btn-primary text-white mt-4 px-5" type="button" onclick="modificarPass(${indexCliente})">Cambiar Password</button>`;
                //No es la contraseña
            } else if (pass != listadoClientes[indexCliente].pass) {
                sweetAlertSinBtn('error', 'Contraseña incorrecta');
                return false;
                //bien la contraseña, pasa el id
            } else if (pass === listadoClientes[indexCliente].pass) {
                return listadoClientes[indexCliente].id;
            }
        } else {
            sweetAlertSinBtn('error', 'Usuario o contraseña incorrectos');
            return false;
        }
    }
}


//FUNCIONES DEL LOGIN
//Login - Admin y usuario - Cambio de contraseña del cliente cuando ingresa por primera vez
function loginUsuario() {
    let usuario = document.getElementById("input_usuario").value;
    let pass = document.getElementById("input_password").value;
    if (checkInputs(usuario, pass)) {
        if (!checkIfAdmin(usuario, pass)) {
            let clientIndex = checkIfClientExists(usuario, pass);
            if (clientIndex !== false) {
                let usuarioLogueado = traerLocalStorage('usuarioLogueado');
                if (!usuarioLogueado) {
                    usuarioLogueado = [];
                }
                usuarioLogueado.push(clientIndex);
                guardarLS('usuarioLogueado', usuarioLogueado);
                limpiarLogin();
                location.href = "clientes.html";
            }
        }
    }
    limpiarLogin();
}

//Funcion que modifica la Pass del usuario la primera vez que ingresa y la guarda en LS
function modificarPass(usuarioIndex) {
    let nuevaPass = document.getElementById('input_nuevaPass').value;
    if (!nuevaPass) {
        sweetAlertSinBtn('error', 'Ingresa una nueva contraseña!');
        nuevaPass = '';
    } else {
        if (nuevaPass.length < 6) {
            sweetAlertSinBtn('error', 'La contraseña debe tener al menos 6 caracteres');
        } else {
            let listadoClientes = traerLocalStorage('clientes');
            listadoClientes[usuarioIndex].pass = nuevaPass;
            guardarLS('clientes', listadoClientes);
            sweetAlertSinBtn('success', 'Contraseña actualizada correctamente');
            setTimeout(() => location.href = "login.html", 2500);
        }
    }
}

//Funcion que limpia el usuario logueado antes de comenzar a loguearse
function limpiarUsuarioLogueadoLS() {
    localStorage.removeItem('usuarioLogueado');
}
limpiarUsuarioLogueadoLS();

document.querySelector("#input_usuario").addEventListener("input", () => {
    let dni = document.getElementById("input_usuario").value;
    var letters = /^[0-9]+$/;
    if (dni.match(letters) === null && dni !== "" && document.querySelector("#userAlert").classList.value.search("d-relative") === -1) {
        document.querySelector("#userAlert").classList.toggle("d-relative");
        document.querySelector("#userAlert").classList.toggle("d-none");
    } else if (dni.match(letters) !== null && dni !== "" && document.querySelector("#userAlert").classList.value.search("d-relative") !== -1) {
        document.querySelector("#userAlert").classList.toggle("d-relative");
        document.querySelector("#userAlert").classList.toggle("d-none");
    } else if (dni === "" && document.querySelector("#userAlert").classList.value.search("d-relative") !== -1) {
        document.querySelector("#userAlert").classList.toggle("d-relative");
        document.querySelector("#userAlert").classList.toggle("d-none");
    }
})