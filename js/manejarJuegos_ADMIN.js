//FUNCIONES GLOBALES
//Funcion generar ID para clases
function generarId(length) {
  let id = "";
  let characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  let charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    id += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return id;
}

//Funciones generales de local storage
function traerLocalStorage(nombreLS) {
  return JSON.parse(localStorage.getItem(nombreLS));
}

function guardarLS(nombreLS, nombreArray) {
  localStorage.setItem(nombreLS, JSON.stringify(nombreArray));
}

//Funciones sweet alert
function sweetAlertSinBtn(icono, mensaje) {
  Swal.fire({
    icon: icono,
    title: mensaje,
    showConfirmButton: false,
    timer: 1500
  });
}

//FUNCIONES PAG: manejar Juegos

//INICIO JUEGO

class juego {
  constructor(juego, tipoJuego, mentor, cupoMaximo, requiereVr, imgURL) {
    this.id = generarId(20);

    this.juego = juego;
    this.tipoJuego = tipoJuego;
    this.mentor = mentor;
    this.requiereVr = requiereVr;
    this.cupoMaximo = cupoMaximo;
    this.imgURL = imgURL;
  }
}

//Función que agrega un nuevo juego al LS. Controla que el cupo sea acorde a la sala. Cierra modal
//Controla: campos vacíos y cupos. No controla nombre del juego repetido.
function agregarJuego() {
  let nombreJuego = document.getElementById('juego').value;
  let tipoJuego = document.getElementById('tipoJuego').value;
  let mentor = document.getElementById('mentor').value;
  let requiereVr = document.getElementById('requiereVr').checked;
  let cupoMaximo = document.getElementById('cupoMaximo').value;
  let imgURL = document.getElementById('imgUrl').value;

  if (nombreJuego == "" || mentor == "" || imgURL == "") {
    sweetAlertSinBtn('error', 'Complete todos los campos');
  } else if (requiereVr && cupoMaximo > 4) {
    sweetAlertSinBtn('error', 'Para los juegos VR el cupo máximo no puede ser mayor a 4');

  } else if (!requiereVr && cupoMaximo > 30) {
    sweetAlertSinBtn('error', 'Para los juegos de PC el cupo máximo no puede ser mayor a 30');
  } else {
    let nuevoJuego = new juego(nombreJuego, tipoJuego, mentor, cupoMaximo, requiereVr, imgURL);
    let juegosLS = traerLocalStorage('juegos');
    if (!juegosLS) {
      juegosLS = [];
    }
    juegosLS.push(nuevoJuego);
    guardarLS('juegos', juegosLS);
    sweetAlertSinBtn('success', `El juego ${nombreJuego} fue correctamente creado y está disponible para sumar a la grilla de horarios`);
    cerrarYvaciarAddJuego();
  }
}

//Funcion que completa el modal de tipos de juego en el modal de agregar juego
function selectTipoJuegoModal() {
  let tiposJuego = traerLocalStorage('tipoJuego');
  if (!tiposJuego) {
    sweetAlertSinBtn('error', 'Agregue primero un tipo de juego');
    //modal no cierra
  } else {
    $('#AddJuego').modal('toggle');
    $('#AddJuego').modal('show');
    let selectTipoJuego = document.getElementById('tipoJuego');
    selectTipoJuego.innerHTML = "";
    for (let i = 0; i < tiposJuego.length; i++) {
      let nuevoSelectOption = document.createElement('option');
      nuevoSelectOption.innerText = tiposJuego[i].nombre;
      nuevoSelectOption.setAttribute("value", tiposJuego[i].nombre);
      selectTipoJuego.appendChild(nuevoSelectOption);
    }
  }
}


//Función que cierra y vacía modal de agregar Juego. Se usa en fcion AgregarJuego
function cerrarYvaciarAddJuego() {
  $('#AddJuego').modal('hide');
  document.getElementById('juego').value = "";
  document.getElementById('mentor').value = "";
  document.getElementById('cupoMaximo').value = "1";
}
//FIN JUEGO

//INICIO TIPO JUEGO
class tipoJuego {
  constructor(nombre) {
    this.id = generarId(20);
    this.nombre = nombre;
  }
}

//Función que agrega un nuevo juego al LS. Controla que el cupo sea acorde a la sala. Cierra modal
//Controla: campos vacíos y cupos. No controla nombre del juego repetido.
function agregarJuego() {
  let nombreJuego = document.getElementById('juego').value;
  let tipoJuego = document.getElementById('tipoJuego').value;
  let mentor = document.getElementById('mentor').value;
  let requiereVr = document.getElementById('requiereVr').checked;
  let cupoMaximo = document.getElementById('cupoMaximo').value;
  let imgURL = document.getElementById('imgUrl').value;
  if (nombreJuego == "" || mentor == "" || imgURL == '' || cupoMaximo == '') {
    sweetAlertSinBtn('error', 'Complete todos los campos');
  } else if (requiereVr && cupoMaximo > 4) {
    sweetAlertSinBtn('error', 'Para los juegos VR el cupo máximo no puede ser mayor a 4');

  } else if (!requiereVr && cupoMaximo > 30) {
    sweetAlertSinBtn('error', 'Para los juegos de PC el cupo máximo no puede ser mayor a 30');
  } else {
    let nuevoJuego = new juego(nombreJuego, tipoJuego, mentor, cupoMaximo, requiereVr, imgURL);
    let juegosLS = traerLocalStorage('juegos');
    if (!juegosLS) {
      juegosLS = [];
    }
    juegosLS.push(nuevoJuego);
    guardarLS('juegos', juegosLS);
    sweetAlertSinBtn('success', `El juego ${nombreJuego} fue correctamente creado y está disponible para sumar a la grilla de horarios`);
    cerrarYvaciarAddJuego();
  }
}
//Funcion que agrega categoria y la almacena en LS.
//Controla: no se repita nombre del tipo de juego
function agregarCategoria() {
  let tiposJuego = traerLocalStorage('tipoJuego');
  if (!tiposJuego) {
    tiposJuego = [];
  }
  let tipoJuegoNombre = document.getElementById('input_categoria').value;
  if (tipoJuegoNombre == "") {
    sweetAlertSinBtn('error', 'Ingrese un tipo de juego');
  } else {
    let existe = tiposJuego.find(elemento => elemento.nombre == tipoJuegoNombre);
    if (existe) {
      sweetAlertSinBtn('error', 'Ese tipo de juego ya existe');
    } else {
      let nuevoTipoJuego = new tipoJuego(tipoJuegoNombre);
      tiposJuego.push(nuevoTipoJuego);
      guardarLS('tipoJuego', tiposJuego);
      cerrarYvaciarAgregarCategoria();
      sweetAlertSinBtn('success', 'El tipo de juego ' + tipoJuegoNombre + " ha sido creado");
    }
  }
}

//Funcion que cierra y vacía modal agregarCategoria.
//Se usa en fcion agregarCategoria
function cerrarYvaciarAgregarCategoria() {
  $('#AddCategory').modal('hide');
  document.getElementById('input_categoria').value = "";
}

//Funcion que carga el select segun uso de VR o no. Se llama en la semana - ventana admin
function cargarSelect(esVR) {
  let juegos = traerLocalStorage('juegos');
  let juegosFiltrados = [];

  if (esVR) {
    juegosFiltrados = juegos.filter(juego => juego.requiereVr == true);
  } else {
    juegosFiltrados = juegos.filter(juego => juego.requiereVr == false);
  }

  let selectJuegos = document.getElementById('juegos');
  selectJuegos.innerHTML = "";
  for (let i = 0; i < juegosFiltrados.length; i++) {
    let newSelect = document.createElement('option');
    newSelect.innerText = juegosFiltrados[i].juego;
    selectJuegos.appendChild(newSelect);
  }
}
//FIN TIPO JUEGO

function checkUserIsLogged() {
  let usuarioLogueado = traerLocalStorage('usuarioLogueado');
  if (!usuarioLogueado) {
    sweetAlertSinBtn("error", "Debes estar logueado para ingresar");
    setTimeout(() => location.href = "login.html", 700);
    console.log('hola')
  }
}