let position = 0;
let colorPosition = 0;
let colorarray = ['#F3F781', '#D8F781', '#9FF781', '#81F79F', '#81F7D8', '#81BEF7', '#8181F7', '#DA81F5', '#F78181', '#FAAC58']
//Funcion que cambia de color la nav y el footer
$(window).scroll(function () {

  if ($("#menu").offset().top == 0) {
    $('#menu').attr('style', `background-color: #ed9239;`);
    $('footer').attr('style', `background-color: #ed9239; font-family: gamer; font-size: 12px;`);
  } else if ($("#menu").offset().top % 50 == 0) {
    let newPosition = window.scrollY;
    if (newPosition > position) {
      if (colorPosition == colorarray.length - 1) {
        position = window.scrollY;
        colorPosition = 0;
      } else {
        position = window.scrollY;
        colorPosition++;
      }
    } else {
      position = window.scrollY;
      if (colorPosition != 0) {
        colorPosition--;
      } else {
        colorPosition = colorarray.length - 1;
      }
    }
    colorRnd = colorarray[colorPosition];
    $('#menu').attr('style', `background-color: ${colorRnd}`);
    $('footer').attr('style', `background-color: ${colorRnd}; font-family: gamer; font-size: 12px;`);
  }
});

