//Funciones generales de local storage
function traerLocalStorage(nombreLS) {
    return JSON.parse(localStorage.getItem(nombreLS));
}

function guardarLS(nombreLS, nombreArray) {
    localStorage.setItem(nombreLS, JSON.stringify(nombreArray));
}


//Funciones sweet alert
function sweetAlertSinBtn(icono, mensaje) {
    Swal.fire({
        icon: icono,
        title: mensaje,
        showConfirmButton: false,
        timer: 1500
    });
}

//Funcion que devuelve un cliente por el id
function getClienteById(id) {
    let clientes = traerLocalStorage('clientes');
    let cliente = clientes.find(elemento => elemento.id === id);
    return cliente;
}

//Funcion que consigue la cantidad de horasSemanalesDisponibles de un cliente por el id
function getHorasDisponiblesClienteById(id) {
    let cliente = getClienteById(id);
    return cliente.horasSemanalesDisponibles - cliente.horasSemanalesOcupadas;
}

//Funcion que agrega el id de una clase a clasesSemana de un cliente y suma 1 a horasSemanalesOcupadas o elimina clase y resta 1 a horasOcupadas
function gestionarClase(inscribir, idClase, idCliente) {
    let clientes = traerLocalStorage('clientes');
    let indexCliente = clientes.find(elemento => elemento.id === idCliente);
    if (inscribir) {
        clientes[indexCliente].horasSemanalesOcupadas++;
        if (!clientes[indexCliente].clasesSemana) {
            clientes[indexCliente].clasesSemana = [];
        }
        clientes[indexCliente].clasesSemana.push(idClase);
        sweetAlertSinBtn('success', 'Te has inscripto satisfactoriamente a la clase');

    } else {
        let indexClase = clientes[indexCliente].clasesSemana.find(elemento => elemento === idClase);
        clientes[indexCliente].horasSemanalesOcupadas--;
        clientes[indexCliente].clasesSemana.splice(indexClase, 1);
        sweetAlertSinBtn('success', 'Te has borrado satisfactoriamente de la clase');
    }
    guardarLS('clientes', clientes);
}

//FUNCIONES DEL LADO DEL CLIENTE

//Funcion que pone el nombre del usuario en la nav
function getNombreUsuario() {
    let usuarioById = traerLocalStorage('clientes').find(cliente => cliente.id === traerLocalStorage('usuarioLogueado')[0]);
    document.getElementById('Usuario').innerText = usuarioById.nombre + ' ' + usuarioById.apellido;
}

//Funcion que trae cuantos turnos le quedan disponibles al cliente
//Consigue los datos desde el LS
function turnosRestantes() {
    let cliente = traerLocalStorage('clientes').find(cliente => cliente.id === traerLocalStorage('usuarioLogueado')[0]);
    let membresia = 0;
    switch (cliente.membresia) {
        case 'Bronce': membresia = 2; break;
        case 'Plata': membresia = 3; break;
        case 'Oro': membresia = 4; break;
        case 'Champion': membresia = 6; break;
    }
    let turnosRestantes = membresia - cliente.clasesSemana.length;
    if (turnosRestantes > 0) {
        document.getElementById('vidas').innerText = 'X ' + turnosRestantes;
    } else {
        document.getElementById('vidas').innerText = 'X 0';
        noTieneTurnosRestantes();
    }
}

//Funcion accesoria que transforma la membresia en horas disponibles
function switchMembresia(membresiaCliente) {
    let membresia;
    switch (membresiaCliente) {
        case 'Bronce': membresia = 2; break;
        case 'Plata': membresia = 3; break;
        case 'Oro': membresia = 4; break;
        case 'Champion': membresia = 6; break;
    }
    return membresia;
}

//Funcion que se ejecuta cuando el usuario no tiene más turnos disponibles: Game over
function noTieneTurnosRestantes() {
    let divTurnos = document.getElementById('divTurnos');
    divTurnos.innerHTML = ``;
}

//Funcion accesoria que devuelve un string con el nombre del dia sigte
function switchDay(dia) {
    let diaSigte;
    switch (dia) {
        case 1: diaSigte = 'Martes'; break;
        case 2: diaSigte = 'Miércoles'; break;
        case 3: diaSigte = 'Jueves'; break;
        case 4: diaSigte = 'Viernes'; break;
        case 5: diaSigte = 'Sábado'; break;
        case 6: diaSigte = 'Domingo'; break;
        case 7: diaSigte = 'Lunes'; break;
    }
    return diaSigte;
}

//Funcion que trae los turnos del dia siguiente. Si es sabado trae los del lunes
function turnosDiaSigte() {
    let turnosAsignados = traerLocalStorage('turnos');
    if (!turnosAsignados) {
        turnosAsignados = [];
    }
    let dia = new Date().getDay();
    let diaSigte = switchDay(dia);
    let findDia = turnosAsignados.find(turno => turno.dia == diaSigte);
    if (!findDia) {
        document.getElementById('gameOver').innerHTML = `<h5 style="font-family: gamer;">No hay turnos para el dia de mañana</h5><img src="./img/gameover.gif" style="max-width: 300px;" alt="">`;
    } else {
        let turnosFiltradosPorDiaSgte = turnosAsignados.filter(turno => turno.dia == diaSigte);
        if (turnosFiltradosPorDiaSgte.length == 0) {
            document.getElementById('gameOver').innerHTML = `<h5 style="font-family: gamer;">No hay turnos para el dia de mañana</h5><img src="./img/gameover.gif" style="max-width: 300px;" alt="">`;
        } else {
            let divTurnos = document.getElementById('divTurnos');
            divTurnos.innerHTML = '';

            for (let i = 0; i < turnosFiltradosPorDiaSgte.length; i++) {
                if (turnosFiltradosPorDiaSgte[i].cupoDisponible == 0) {
                    divTurnos.innerHTML += crearTurnoVacio(turnosFiltradosPorDiaSgte[i]);
                }
                else {
                    divTurnos.innerHTML += crearTurnoConCupo(turnosFiltradosPorDiaSgte[i]);
                }
            }
        }

    }


}

//Funcion que crea un turno sin cupo y lo retorna
function crearTurnoVacio(turno) {
    let juegos = traerLocalStorage('juegos');
    let index = juegos.findIndex(juego => juego.juego == turno.juego);
    let imgUrl = juegos[index].imgURL;

    let img = turno.sala == "Sala VR" ? "./img/headset.png" : "./img/monitor.png";
    return `<!--Inicio Card-->  
    <div class="card text-white text-center mx-auto mb-5" style="width: 270px; background: transparent;">
      <div class=" mb-0" style="background-color: #ff971f;">
        <img src="${imgUrl}" alt="" style="height: 10rem; width: 100%; ">
        <p class=" bg-primary text-white" style="color:orangered;">${turno.hora}</p>
        <h5 class=" font-weight-bold mt-1 text-white" style="color:orangered; font-size: 18px;">${turno.juego}<img class="ml-2" src="${img}" style=" max-width: 50px;" alt=""></h5>
        <footer id="menu" class="mt-2 text-white" style="background: red;">
          <p style="color: black; font-family: Arial, Helvetica, sans-serif;">Sin cupo disponible</p>
          <button class="bg-primary border infinite border-0 text-white mx-5 mb-2 px-3 py-1 text-center rounded-pill"
          style="font-size: 15px; disabled"><i class="fas fa-caret-right mr-2"></i>Agregar<i
            class="fas fa-caret-left ml-2"></i></button>
        </footer>
      </div>
    </div>
    <!--Fin Card-->`;
}

//Funcion que crea un turno con cupo disponible
function crearTurnoConCupo(turno) {
    let juegos = traerLocalStorage('juegos');
    let index = juegos.findIndex(juego => juego.juego == turno.juego);
    let imgUrl = juegos[index].imgURL;
    let usuarioLogueado = traerLocalStorage('clientes').find(cliente => cliente.id === traerLocalStorage('usuarioLogueado')[0]);;

    let turnos = traerLocalStorage('turnos');
    let turnoElegido = turnos.filter(elemento => elemento.id == turno.id);
    let usuarioEnClase = turnoElegido[0].alumnos.findIndex(alumno => alumno == `${usuarioLogueado.nombre} ${usuarioLogueado.apellido}`);
    let disabled = usuarioEnClase != -1 ? `<p class=' mx-2 mb-2 px-3 pb-4' style="padding-top: 21px;color: black;font-family: Arial, Helvetica, sans-serif;">Ya estás inscripto a esta clase</p>` : `<button class="animated pulse bg-primary border infinite mt-4 border-0 text-white mx-5 px-3 py-1 text-center rounded-pill" style="font-size: 15px; margin-bottom: 15px" onclick="asignarTurno('${turno.id}')"><i class="fas fa-caret-right mr-2"></i>Agregar<i class="fas fa-caret-left ml-2"></i></button>`;

    let img = turno.sala == "Sala VR" ? "./img/headset.png" : "./img/monitor.png";
    return `<!--Inicio Card-->  
    <div class="card text-white text-center border-0 mx-auto mb-5" style="width: 270px; background: transparent;">
      <div class="mb-0" style="background-color: #ff971f;">
        <img src="${imgUrl}" alt="" style="height: 10rem; width: 100%;">
        <p class=" bg-primary text-white" style="color: orangered;">${turno.hora}</p>
        <h5 class=" font-weight-bold mt-1 text-white mb-0" style="color: orangered; font-size: 18px;">${turno.juego}<div><img class="ml-2 my-2" src="${img}" style=" max-width: 50px;" alt=""></div></h5></div>
        <footer id="menu" class="mb-4" style="color: black; background-color: #ff971f; style="font-family: Arial, Helvetica, sans-serif;">
          <p class="pt-2" style=" color: black; font-family: Arial, Helvetica, sans-serif;">Cupo disponible: ${turno.cupoDisponible}</p>
          ${disabled}
        </footer>
      </div>
    </div>
    <!--Fin Card-->`;
}

//Funcion que crea un nuevo turno al listado de turnos del usuario.Chequea si el cliente ya esta inscripto en esa clase
function asignarTurno(idTurno) {
    let clientes = traerLocalStorage('clientes');
    let usuarioLogueado = clientes.find(cliente => cliente.id === traerLocalStorage('usuarioLogueado')[0]);
    let turnos = traerLocalStorage('turnos');
    let indexUsuario = clientes.findIndex(cliente => cliente.id === usuarioLogueado.id);
    let indexTurno = turnos.findIndex(turno => turno.id === idTurno);
    let nombreUsuario = clientes[indexUsuario].nombre + ' ' + clientes[indexUsuario].apellido;
    let userInTurno = turnos[indexTurno].alumnos.find(alumno => alumno === nombreUsuario);
    if (clientes[indexUsuario].habilitado == false) {
        sweetAlertSinBtn('error', 'No puedes inscribirte si estás suspendido. Contactate con el administrador');
    } else if (userInTurno) {
        sweetAlertSinBtn('error', `Ya estás inscripto en esta clase`);
    } else {
        let userHasClaseInthatTime = clientes[indexUsuario].clasesSemana.find(turno => turno.horario === turnos[indexTurno].hora);
        if (userHasClaseInthatTime) {
            sweetAlertSinBtn('error', `Ya estás inscripto en una clase a las ${turnos[indexTurno].hora}`);
        } else {
            turnos[indexTurno].alumnos.push(`${clientes[indexUsuario].nombre} ${clientes[indexUsuario].apellido}`);
            turnos[indexTurno].cupoDisponible--;
            let nuevaClase = new claseInscripta(turnos[indexTurno].dia, turnos[indexTurno].hora, turnos[indexTurno].juego, turnos[indexTurno].id);
            clientes[indexUsuario].clasesSemana.push(nuevaClase);
            clientes[indexUsuario].horasSemanalesOcupadas++;

            guardarLS('clientes', clientes);
            guardarLS('turnos', turnos);
            turnosCliente();
            turnosRestantes();
            turnosDiaSigte();
            sweetAlertSinBtn('success', `Te inscribiste a la clase de mañana a las ${turnos[indexTurno].hora} para el juego ${turnos[indexTurno].juego}`);
        }
    }
}


//Funcion que trae los turnos ya elegidos del cliente.
//Se muestran en la parte superior de la pantalla del cliente
function turnosCliente() {
    let usuarioLogueado = traerLocalStorage('clientes').find(cliente => cliente.id === traerLocalStorage('usuarioLogueado')[0]);
    let turnos = usuarioLogueado.clasesSemana;
    let membresia = switchMembresia(usuarioLogueado.membresia);
    let turnosDisponibles = membresia - turnos.length;
    let turnosOcupados = turnos.length;

    let divTurnosCliente = document.getElementById('divTurnosCliente');
    divTurnosCliente.innerHTML = '';
    if (turnos.length == 0) {
        for (let i = 0; i < membresia; i++) {
            divTurnosCliente.innerHTML += turnoDisponibleCliente();
        }
    } else {
        for (let i = 0; i < turnosOcupados; i++) {
            divTurnosCliente.innerHTML += turnoOcupadoCliente(turnos[i]);
        }
        for (let i = 0; i < turnosDisponibles; i++) {
            divTurnosCliente.innerHTML += turnoDisponibleCliente();
        }
    }

    if (membresia == turnos.length) {
        let divGameOver = document.getElementById('gameOver');
        divGameOver.innerHTML = '';
        divGameOver.innerHTML = `
        <!--Game over-->
        <p style="font-family: gamer; font-size: 20px; color: #000;">Intenta nuevamente la semana que viene</p>
        <img src="./img/gameover.gif" style="max-width: 300px;" alt="">
        <!--Fin de Game over-->
        `;
        document.getElementById('divTurnos').innerHTML = '';
    }
}

//Funcion que retorna string de turnos que todavía no ocupó el cliente
function turnoDisponibleCliente() {
    return `
    <!--Inicio Card-->
    <div class="card text-white text-center mx-auto" style=" border: 3px dashed orangered; width: 270px; opacity: 60%;">
      <div class=" mb-0">
        <img src="./img/select.gif" alt="" style="height: 12rem; width: 100%; ">
        <div id="menu" class="bg-primary text-white pt-1 pb-2">
          Agrega un juego
        </div>
      </div>
    </div>
    <!--Fin Card-->`;
}

//Funcion que retorna un turno ya saleccionado por cliente. Requiere el index para poder eliminar el turno. Poner que solo traiga el btn eliminar si el curr day es = al dia sgte
function turnoOcupadoCliente(turno) {
    let juegos = traerLocalStorage('juegos');
    let juegoSeleccionado = juegos.find(juego => juego.juego == turno.juego);
    let imgJuego = juegoSeleccionado.imgURL;
    let dia = new Date().getDay();
    let diaSigte = switchDay(dia);
    let btn = diaSigte == turno.dia ? `<button class="btn btn-warning ml-2" onclick="quitarTurno('${turno.id}')"><i class="fas fa-trash"></i></button>` : `<button class="btn btn-secondary disabled ml-2" ><i class="fas fa-trash"></i></button>`;

    return `
    <!--Inicio Card-->
    <div class="card text-center bg-primary mx-auto border-0" style="width: 270px;">
      <img src="${imgJuego}" alt="" style="height: 12rem; width: 100%;">
      <div id="menu" class="bg-primary text-white text-center pt-1 pb-2">
        ${turno.dia}: ${turno.horario}
        ${btn}
      </div>
    </div>
    <!--Fin Card-->`;
}

//Function que elimina la clase de la lista de clases del cliente
function quitarTurno(idTurno) {
    let turnos = traerLocalStorage('turnos');
    let indexTurno = turnos.findIndex(elemento => elemento.id == idTurno);

    let clientes = traerLocalStorage('clientes');
    let usuarioLogueado = clientes.find(cliente => cliente.id === traerLocalStorage('usuarioLogueado')[0]);
    let nombreUsuario = usuarioLogueado.nombre + ' ' + usuarioLogueado.apellido;

    let indexUsuarioLogueado = clientes.findIndex(cliente => cliente.id === traerLocalStorage('usuarioLogueado')[0]);
    let indexTurnoEnUsuario = clientes[indexUsuarioLogueado].clasesSemana.findIndex(turno => turno.id === idTurno);

    let indexUsuarioEnTurno = turnos[indexTurno].alumnos.findIndex(alumno => alumno === nombreUsuario);
    turnos[indexTurno].alumnos.splice(indexUsuarioEnTurno, 1);
    clientes[indexUsuarioLogueado].clasesSemana.splice(indexTurnoEnUsuario, 1)
    turnos[indexTurno].cupoDisponible++;

    guardarLS('clientes', clientes);
    guardarLS('turnos', turnos);
    turnosCliente();
    turnosDiaSigte();
    turnosRestantes();
    sweetAlertSinBtn('success', 'Clase eliminada');
}

//Class clase inscripta que almacena los datos de las clases del cliente
class claseInscripta {
    constructor(dia, horario, juego, idTurno) {
        this.id = idTurno;
        this.dia = dia;
        this.horario = horario;
        this.juego = juego;
    }
}

function limpiarLsUsuarioLogueado() {
    localStorage.removeItem('usuarioLogueado');
    setTimeout(() => location.href = "inicio.html", 70);
}

function checkUserIsLogged() {
    let usuarioLogueado = traerLocalStorage('usuarioLogueado');
    if (!usuarioLogueado) {
        sweetAlertSinBtn("error", "Debes estar logueado para ingresar");
        setTimeout(() => location.href = "login.html", 700);
        console.log('hola')
    }
}

let nombre = "Gym Esperanza";
let colors = [
    "#4C1A7E", "#7E1A7E", "#7E1A4C", "#7E1A1A", "rgb(128, 74, 21)", "#72720E", "rgb(65, 110, 20)",
    "#1C741C", "#26724C", "#267272", "#264C72", "#264C72", "#264C72"
]

function setGymName() {
    let nombre = "Gym Esperanza";
    let colors = [
        "#4C1A7E", "#7E1A7E", "#7E1A4C", "#7E1A1A", "rgb(128, 74, 21)", "#72720E", "rgb(65, 110, 20)",
        "#1C741C", "#26724C", "#267272", "#264C72", "#264C72"
    ]
    let gymNameDiv = document.querySelector("#gymName");
    for (let i = 0; i < nombre.length; i++) {
        if (nombre[i] === " ") {
            gymNameDiv.innerHTML += "&nbsp;"
        } else {
            gymNameDiv.innerHTML += `<label style="font-size: medium ; font-family: gamer; color: ${colors[i]}; text-shadow:  0 0 12px orange;" >${nombre[i]}</label>`
        }
    }
}


