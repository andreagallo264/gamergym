//Funciones generales de local storage
function traerLocalStorage(nombreLS) {
    return JSON.parse(localStorage.getItem(nombreLS));
}

function guardarLS(nombreLS, nombreArray) {
    localStorage.setItem(nombreLS, JSON.stringify(nombreArray));
}

//Funciones sweet alert
function sweetAlertSinBtn(icono, mensaje) {
    Swal.fire({
        icon: icono,
        title: mensaje,
        showConfirmButton: false,
        timer: 1500
    });
}

//Listado de juegos en el ADMIN
function listarTurnos() {
    let divTurnos = document.getElementById('turnos');
    divTurnos.innerHTML = '';
    let turnos = traerLocalStorage('turnos');
    if (!turnos) {
        turnos = [];
    }
    let juegos = traerLocalStorage('juegos');
    if (turnos.length > 0) {
        for (let i = 0; i < turnos.length; i++) {
            let img = juegos.find(elemento => elemento.juego == turnos[i].juego).imgURL;
            let card = `<!--Inicio Card-->
        <div class="card text-center bg-primary mx-auto border-0" style="width: 270px;">
          <img src="${img}" alt="" style="height: 12rem; width: 100%;">
          <div class="bg-primary text-white pt-2 pb-1">
            <p>${turnos[i].dia} ${turnos[i].hora}</p>
            <p>${turnos[i].juego} ${turnos[i].sala === "Sala VR" ? '<img style="width: 30px;" src="./img/headset.png" />' : ""}</p>
            <button type='submit' class='btn btn-warning px-5' onclick="eliminarTurnoListado('${turnos[i].id}')"><i class="fas fa-trash"></i></button>
          </div>
        </div>
        <!--Fin Card-->`;
            divTurnos.innerHTML += card;
        }
    } else {
        sweetAlertSinBtn("error", "No tiene turnos creados")
    }
}

//Funcion que elimina los turnos desde la pagina de listarTurnos
function eliminarTurnoListado(id) {
    let turnos = traerLocalStorage('turnos');
    let index = turnos.findIndex(turno => turno.id == id);
    if (turnos[index].alumnos.length == 0) {
        turnos.splice(index, 1);
        guardarLS('turnos', turnos);
        listarTurnos();
        sweetAlertSinBtn('success', 'Turno eliminado');
    } else {
        sweetAlertSinBtn('error', 'No podés borrar un turno con alumnos inscriptos!');
    }

}

//Funcion que genera los botones de los horarios con las funciones que traen la info de las salas
//Se llama en generarAcordeones
function generarHorarios(dia, idCardBody) {
    let cardBody = document.getElementById(idCardBody);
    for (let i = 0; i < horarios.length; i++) {
        let btn = document.createElement("button");
        btn.innerText = horarios[i];
        btn.setAttribute('class', 'btn btn-primary mx-2 my-1');
        btn.setAttribute('onclick', `generarSalas('${dia}', '${horarios[i]}')`);
        cardBody.appendChild(btn);
    }

}

function checkUserIsLogged() {
    let usuarioLogueado = traerLocalStorage('usuarioLogueado');
    console.log("eeeh")
    if (!usuarioLogueado) {
        sweetAlertSinBtn("error", "Debes estar logueado para ingresar");
        setTimeout(() => location.href = "login.html", 700);
        console.log('hola')
    }
}