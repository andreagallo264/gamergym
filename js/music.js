//Musica
function playMusic(musica, btnMusica) {
    let audios = ['mario', 'pokemon', 'dragonball'];
    let btnAudios = ['btnMario', 'btnPokemon', 'btnDragonball'];
    for (let i = 0; i < audios.length; i++) {
        if (audios[i] != musica) {
            let audio = document.getElementById(audios[i]);
            let btn = document.getElementById(btnAudios[i]);
            if (btn.getAttribute('onclick') == `pauseMusic('${audios[i]}', '${btnAudios[i]}')`) {
                btn.setAttribute('onclick', `playMusic('${audios[i]}', '${btnAudios[i]}')`);
                document.getElementById(audios[i]).pause();
            }
        }
    }
    document.getElementById(musica).play()
    document.getElementById(btnMusica).setAttribute('onclick', `pauseMusic('${musica}', '${btnMusica}')`);
}
function pauseMusic(musica, btnMusica) {
    document.getElementById(musica).pause();
    document.getElementById(btnMusica).setAttribute('onclick', `playMusic('${musica}', '${btnMusica}')`);
}